# Médecins Sans Frontières: Forecasting demand of medical equipment
MSF provides medical humanitarian assistance in more than 70 countries across the world.  The supply centers guarantee the safety of medical and non-medical supplies, and deliver what the missions need, where and when they need it. To this end, MSF Supply manages a catalogue consisting of 13.000 items. A better resource allocation and inventory management means that the resources arrive at the time and quantity required. The goal of this project is to forecast demand for products in order to better allocate resources when taking inventory decisions, to avoid having excess stock or stock-outs. By identifying and understanding patterns in ordering behaviour and drivers of demand of the missions, MSF expects to increase the explainability and improve in anticipating the needs of the missions. 

## Recommended environment

Editor: Visual Studio Code

VSCode Extensions:

- [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)

VSCode Settings (access with `CMD + ,` and search):

- editor.formatOnSave = true
- python.formatting.provider = 'yapf'

## Installation

- Install [Python](https://www.python.org/downloads/), [pip](https://www.python.org/downloads/), and [pipenv](https://pipenv.pypa.io/en/latest/)
- Run `pipenv install`
- Select the newly created virtual environment as the Python interpreter (`CMD + SHIFT + P` and `Python: Select Interpreter`)
- Download data as csv and add it to /data/main.csv
- Run Python code in VSCode's Python Interactive Mode by selecting the desired rows to run and pressing `SHIFT + ENTER`

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:c8025276be15008bbf49a9722415d683?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:c8025276be15008bbf49a9722415d683?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:c8025276be15008bbf49a9722415d683?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/analytics-club/hack4good/hack4good-fall-2021/m-decins-sans-fronti-res/medecins-sans-frontieres.git
git branch -M main
git push -uf origin main
```

---

## Forecasting demand of medical equipment

## Project Description

[Link](https://bit.ly/3urNj4R)

## Important links

[Polybox](https://polybox.ethz.ch/index.php/f/2556781981)  
[Gitlab (you're already there ;) )](https://gitlab.com/analytics-club/hack4good/hack4good-fall-2021/m-decins-sans-fronti-res)  
[Welcome booklet](https://bit.ly/3kTYwbi)

## Team

**H4G project responsibles:** Cecilia cecilia.valenzuela@analytics-club.org  
**NGO representative:** Eugenie Cochin  
**Mentor:** Yevgeniy Ilyin ilyiny@amazon.ch

**Team members:**

- Kathrin Durizzo
- Frithiof Ekström
- Carlos García Meixide
- Jonathan Koch
