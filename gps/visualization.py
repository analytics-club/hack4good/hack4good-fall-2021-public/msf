#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 13:07:56 2021

@author: carlos
"""

#!/usr/bin/env python
# coding: utf-8

# In[50]:


import os
import numpy as np
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error, r2_score
import random
import GPy

GPy.plotting.change_plotting_library('matplotlib')

get_ipython().run_line_magic('cd', '"~/predictions/"')


# Print the ArticleVersion of the items for which we have a time series

# In[3]:


import glob, os
lista=glob.glob("*.csv")

lista.index(' DORAMETN5T-- .csv')


for file in lista:
    print(file)
    
    
    
df= pd.read_csv(' SINSNEED21-- .csv', parse_dates=False, header=0,sep=',' )
df=df.sort_values(by=['Numdate'])
 
X=df['Numdate'].values.reshape(len(df),1)
Y=df['Logorder'].values.reshape(len(df),1)
Z=df['Prediction'].values.reshape(len(df),1)


h=90
Sm=df['Logorder'].rolling(h).sum()/h
Sl=Sm.values.reshape(len(df),1)
S=Sl[h:len(Y)]
X=X[h:len(Y)]
Z=Z[h:len(Y)]



plt.figure(figsize=(20,10))
plt.plot(X, Z,linewidth=1,color='red')
plt.plot(X, S,linewidth=1,color='black')

plt.title(file)