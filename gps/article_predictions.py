#!/usr/bin/env python
# coding: utf-8

# In[50]:


import os
import numpy as np
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error, r2_score
import random
import GPy

GPy.plotting.change_plotting_library('matplotlib')

get_ipython().run_line_magic('cd', '"~/morethan600/"')


# Print the ArticleVersion of the items for which we have a time series

# In[3]:


import glob, os
lista=glob.glob("*.csv")

lista.index(' DORAMETN5T-- .csv')


for file in lista[56:len(lista)]:
    print(file)


# We plot the demand time series for the articles above (warnings are only about excesive memory usage for plotting).
# Just scroll down to see the plots

# In[4]:


# for file in glob.glob("*.csv"):
#     df= pd.read_csv(file, parse_dates=False, header=0, usecols=[1,2,3],sep=' ' )
#     df=df.sort_values(by=['Numdate'])
    
#     X=df['Numdate'].values.reshape(len(df),1)
#     Y=df['Logorder'].values.reshape(len(df),1)

#     h=90
#     Sm=df['Logorder'].rolling(h).sum()/h
#     Sl=Sm.values.reshape(len(df),1)
#     S=Sl[h:len(Y)]
#     X=X[h:len(Y)]
   

#     plt.figure(figsize=(20,10))
#     plt.plot(X, S,linewidth=1,color='black')
#     plt.title(file)


# We pick one article we like **START LOOP HERE**

# In[71]:


for file in lista:

    get_ipython().run_line_magic('cd', '"~/morethan600/"')    
    df= pd.read_csv(file, parse_dates=False, header=0, usecols=[1,2,3],sep=' ' )
    df=df.sort_values(by=['Numdate'])
    
    X=df['Numdate'].values.reshape(len(df),1)
    Y=df['Logorder'].values.reshape(len(df),1)
    
    h=90
    Sm=df['Logorder'].rolling(h).sum()/h
    Sl=Sm.values.reshape(len(df),1)
    S=Sl[h:len(Y)]
    X=X[h:len(Y)]
    
    
    plt.figure(figsize=(20,10))
    plt.plot(X, S,linewidth=1,color='black')
    plt.title(file)
    
    
    
    Xf=X.squeeze(-1)
    Xf.ndim
    
    Sf=S.squeeze(-1)
    Sf.ndim
    
    
    # shrink stores how many times bigger is the original time domain with respect to the one of the 'homogeneized' series
    shrink=(Xf[-1]-Xf[0])/len(Xf)
    shrink 
    
    
    
    
    
    
    
    # Now let us detrend the data (polynomial spline should be enough)
    
    # In[72]:
    
    
    pf = PolynomialFeatures(degree=4)
    Xp = pf.fit_transform(Xf.reshape(-1, 1)) # it is the input shape the library asks you 
    md2 = LinearRegression()
    md2.fit(Xp, Sf)
    trendp = md2.predict(Xp)
    
    
    plt.plot(Xf, Sf)
    plt.plot(Xf, trendp)
    plt.legend(['data', 'polynomial trend'])
    plt.show()
    
    S=Sf-trendp # zm stands for zero-mean
    
    plt.plot(Xf, S)
    plt.show()
    
    
    # We choose the peak frequencies (the 10 with highest complex module)
    
    # In[73]:
    
    
    import scipy
    
    
    yf = np.fft.fft(S)
    freq = np.fft.fftfreq(S.shape[-1])
    
    # x-axis are wavenumbers k=2pi/lambda
    
    plt.figure(figsize=(20,10))
    plt.plot(freq,abs(yf),color='black')
    plt.title('Fourier transform of the homogenized series')
    len(yf)
    
    yf2=yf[np.where(freq>0)]
    freq2=freq[np.where(freq>0)]
    
    
    # In[74]:
    
    
    plt.figure(figsize=(20,10))
    plt.plot(freq2,abs(yf2),color='black')
    plt.title('Fourier transform of the homogenized series')
    
    
    # In[75]:
    
    
    Ti=abs(shrink/freq2[np.argsort(-abs(yf2))])[range(0,15,1)]
    Ti
    
    
    # In[76]:
    
    
    pastdays=np.flip(np.diff(Xf)).cumsum(axis=0)
    
    
    # In[77]:
    
    
    res = next(x for x, val in enumerate(pastdays)
                                      if val > 90)
    train=range(0,(len(X)-res)) # we choose everything but last 3 months for training
    
    
    # In[ ]:
    
    
    objectives=[]
    
    for T in Ti:
    
        ker=GPy.kern.StdPeriodic(1,period=T)
        
        m = GPy.models.GPRegression(X[train],S[train].reshape(-1,1),ker)
    
        # optimize and plot
        m.optimize(messages=True,max_f_eval = 10000)
        print(m)
        
        Yp, Vp = m.predict(X)
        lo, up = m.predict_quantiles(X, quantiles=(2.5, 97.5), Y_metadata=None, kern=None, likelihood=None)
    
        squ=(S[range(train[-1],len(X))].squeeze()-Yp[range(train[-1],len(X))].squeeze())**2
        #objectives.append(m.log_likelihood())
        objectives.append(np.sum(squ))
        #objectives.append(np.sum(np.square(Szm-Yp)))
        
    
    
    # In[66]:
    
    
    objectives
    
    
    # In[68]:
    
    
    #ker=GPy.kern.StdPeriodic(1,period=np.median(Ti))#+GPy.kern.StdPeriodic(1,period=np.min(Ti))#+GPy.kern.StdPeriodic(1,period=Ti[4])+GPy.kern.StdPeriodic(1,period=Ti[2])
    
    ker=GPy.kern.StdPeriodic(1,period=Ti[np.argmin(objectives)])#+GPy.kern.StdPeriodic(1,period=np.median(Ti))
    m = GPy.models.GPRegression(X[train],S[train].reshape(-1,1),ker)
    
    # optimize and plot
    m.optimize(messages=True,max_f_eval = 10000)
    
    
    Yp, Vp = m.predict(X)
    lo, up = m.predict_quantiles(X, quantiles=(2.5, 97.5), Y_metadata=None, kern=None, likelihood=None)
    
    
    # In[69]:
    
    
    plt.figure(figsize=(20,10))
    plt.plot(X,Yp,'black',linewidth=3)
    plt.plot(X[train], S[train], "or")
    plt.plot(X[train[-1]:len(X)], S[train[-1]:len(X)], "o")
    plt.plot(X,lo,'--',color='black')
    plt.plot(X,up,'--',color='black')
    
    
    # In[23]:
    
    
    len(df['Logorder'])
    
    
    # In[24]:
    
    
    len(S)
    
    
    # In[25]:
    
    
    Yp=Yp.squeeze()+trendp.squeeze()
    
    
    # In[26]:
    
    
    plt.figure(figsize=(20,10))
    plt.plot(X,Yp,color='black')
    plt.plot(X, (Sl)[h:len(df['Logorder'])], "or")
    
    
    # In[27]:
    
    
    Yp
    
    
    # In[28]:
    
    
    Scomplete=np.concatenate((np.repeat('NA', h),Yp.reshape(-1)))
    len(Scomplete)
    
    
    # In[29]:
    
    
    df['Prediction']=Scomplete
    
    
    # In[30]:
    
    
    df
    
    
    # In[31]:
    
    
    get_ipython().run_line_magic('cd', '"~/predictions/"')
    df.to_csv (file, index = False, header=True)
    
    
    # In[ ]:




