import pandas as pd
import pathlib
import os
from pandas.core.frame import DataFrame
'''
This code applies the classification found at https://frepple.com/blog/demand-classification/ to the
cleaned dataset. Each item is thereby assigned one of the categories smooth, intermittent, erratic or lumpy.
The classification is applied on different time periods (single years or the whole 5-year period) as well
as different demand bucket sizes (eg. summing up all demands for a month / quarter).
'''

# Years in the observed period, including 'Total' for the whole period
PERIOD = [2016, 2017, 2018, 2019, 2020, 2021, 'Total']


def save_csv(df: pd.DataFrame, filename: str):
  filepath = os.path.join(os.getcwd(), '..', 'data', 'classification')
  pathlib.Path(filepath).mkdir(parents=True, exist_ok=True)
  df.to_csv(os.path.join(filepath, filename))


def CV2(df: DataFrame, item: str, period: str):
  """
    Returns the square of the coefficient of variation of a given item, defined as 
    (standard deviation / average)^2
    """
  if (df.index.value_counts()[item] == 1):
    return 0
  temp = DataFrame(df[[]])
  temp = df.loc[item].groupby(period)
  temp = temp['Ordered quantity'].agg('sum')
  return (temp.std() / temp.mean())**2


def classification(demand_behavior: DataFrame, item: str, adi: str, cv2: str):
  """
    Returns the demand classification of an item (either intermittent, lumpy, smooth or
    erratic) depending on average demand interval the variability in demand quantity. This classification is
    based on https://frepple.com/blog/demand-classification/
    """
  if (demand_behavior['Number of orders'][item] == 1):
    return 'Only ordered once'
  if (demand_behavior[cv2][item] < 0.49):
    if (demand_behavior[adi][item] < 1.32):
      return 'Smooth'
    else:
      return 'Intermittent'
  if (demand_behavior[adi][item] < 1.32):
    return 'Erratic'
  return 'Lumpy'


# Import clean dataset
data_path_clean = os.path.join(os.getcwd(), '..', 'data', 'csv', 'clean.csv')
clean = pd.read_csv(data_path_clean, encoding='ISO-8859-1')

# Split dataset into different years from 2016 to 2021
years = {}
demand = {}
for year in PERIOD:
  years["clean_{0}".format(year)] = clean[clean['Year'] == year]
  years["clean_{0}".format(year)] = years["clean_{0}".format(year)].set_index(
      'ArticleVersionLast')

years['clean_Total'] = clean.set_index('ArticleVersionLast')

for year in years:

  source = years[year]

  # For the calculation of ADI we use the total number of months / quarters in the time interval we are looking at. For
  # a year that equates to 12 and 4, for the whole dataset it is 70 and 24
  if year == 'clean_Total':
    MONTHS = 70
    QUARTERS = 24
  else:
    MONTHS = 12
    QUARTERS = 4

  articles = source.index.unique()

  # Create DataFrame displaying demand patterns for each year
  demand[year] = DataFrame(index=articles,
                           columns=[
                               'Description', 'Family', 'Number of orders',
                               'CV2 (monthly)', 'CV2 (quarterly)',
                               'ADI (monthly)', 'ADI (quarterly)',
                               'Classification (monthly)',
                               'Classification (quarterly)'
                           ])

  for el in demand[year].index:
    demand[year]['Description'][el] = pd.Series(
        source.loc[el]['DescriptionArticleEn']).unique()[0]
    demand[year]['Family'][el] = pd.Series(
        source.loc[el]['Familly name']).unique()[0]
    demand[year]['Number of orders'][el] = source.index.value_counts()[el]
    demand[year]['CV2 (monthly)'][el] = CV2(source, el, 'Month')
    demand[year]['CV2 (quarterly)'][el] = CV2(source, el, 'Quarter')
    demand[year]['ADI (monthly)'][el] = MONTHS / pd.Series(
        source.loc[el]['Date']).nunique()
    demand[year]['ADI (quarterly)'][el] = QUARTERS / pd.Series(
        source.loc[el]['Date_quarter']).nunique()
    demand[year]['Classification (monthly)'][el] = classification(
        demand[year], el, 'ADI (monthly)', 'CV2 (monthly)')
    demand[year]['Classification (quarterly)'][el] = classification(
        demand[year], el, 'ADI (quarterly)', 'CV2 (quarterly)')

    # Save DataFrame as .csv
    save_csv(demand[year], 'classification_{0}.csv'.format(year))

# Create DataFrame that gives overview of demand classification for all years and sorts by smoothness of items on a MONTHLY basis
results_monthly = {}
for year in PERIOD:
  results_monthly["monthly_{0}".format(year)] = demand["clean_{0}".format(
      year)]['Classification (monthly)']

result_monthly = pd.concat(results_monthly.values(), axis=1)
result_monthly = result_monthly.fillna('Not ordered')
result_monthly['Total smooth'] = 0
result_monthly['Years smooth'] = 0
result_monthly.columns = [
    '2016', '2017', '2018', '2019', '2020', '2021', 'Total', 'Total smooth',
    'Years smooth'
]

for el in result_monthly.index:
  if ('Smooth' in result_monthly.loc[el].value_counts().index):
    result_monthly.at[
        el, 'Total smooth'] = result_monthly.loc[el].value_counts()['Smooth']
  if ('Smooth' in result_monthly.loc[el][:-3].value_counts()):
    result_monthly.at[
        el,
        'Years smooth'] = result_monthly.loc[el][:-3].value_counts()['Smooth']

result_monthly = result_monthly.sort_values(by='Total smooth', ascending=False)

save_csv(result_monthly, 'monthly_classification_overview.csv')

# Create DataFrame that gives overview of demand classification for all years and sorts by smoothness of items on a QUARTERLY basis
results_quarterly = {}
for year in PERIOD:
  results_quarterly["quarterly_{0}".format(year)] = demand["clean_{0}".format(
      year)]['Classification (quarterly)']

result_quarterly = pd.concat(results_quarterly.values(), axis=1)
result_quarterly = result_quarterly.fillna('Not ordered')
result_quarterly['Total smooth'] = 0
result_quarterly['Years smooth'] = 0
result_quarterly.columns = [
    '2016', '2017', '2018', '2019', '2020', '2021', 'Total', 'Total smooth',
    'Years smooth'
]

for el in result_quarterly.index:
  if ('Smooth' in result_quarterly.loc[el].value_counts().index):
    result_quarterly.at[
        el, 'Total smooth'] = result_quarterly.loc[el].value_counts()['Smooth']
  if ('Smooth' in result_quarterly.loc[el][:-3].value_counts()):
    result_quarterly.at[el, 'Years smooth'] = result_quarterly.loc[
        el][:-3].value_counts()['Smooth']

result_quarterly = result_quarterly.sort_values(by='Total smooth',
                                                ascending=False)

save_csv(result_quarterly, 'quarterly_classification_overview.csv')