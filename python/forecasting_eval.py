import pandas as pd
from pandas.core.frame import DataFrame
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib
'''
This code serves to evaluate the performance of the different models that have been applied to the
dataset. For each model and time period it counts the number of times a model was the most accurate
in forecasting a product and it evaluates the mean error for each model and timeperiod. Various error
metrics are used, we believe the normalised root mean squared error (NRMS) is most representative of
the performances.
'''


def save_csv(df: pd.DataFrame, filename: str):
  filepath = os.path.join(os.getcwd(), '..', 'data', 'eval')
  pathlib.Path(filepath).mkdir(parents=True, exist_ok=True)
  df.to_csv(os.path.join(filepath, filename))


# Scores to evaluate
data = [
    'RMSE_scores.csv', 'NRMSE_scores.csv', 'MAE_scores.csv', 'R2_scores.csv',
    'MAPE_scores.csv'
]

results = {}
counts = {}

# Periods to evaluate on
scope = ['2016', '2017', '2018', '2019', '2020', '2021', 'Total']

for y in scope:

  for el in data:
    data_path = os.path.join(os.getcwd(), '..', 'data', 'forecasts', el)
    df = pd.read_csv(data_path, encoding='ISO-8859-1')

    data_path = os.path.join(os.getcwd(), '..', 'data', 'classification',
                             'monthly_classification_overview.csv')
    classification = pd.read_csv(data_path, encoding='ISO-8859-1')

    df = df.set_index('Unnamed: 0')
    classification = classification.set_index('ArticleVersionLast')

    df.replace([np.inf], np.nan, inplace=True)
    df.dropna()
    if el == 'NRMSE_scores.csv':
      df = df[df[[
          'Rolling Average', 'ARIMA', 'LinearRegression', 'RandomForest',
          'XGBoost'
      ]].max(axis=1) < 100]

    result = pd.concat([classification, df], axis=1, join="inner")

    count_best = pd.DataFrame(
        index=['Smooth', 'Erratic', 'Intermittent', 'Lumpy'],
        columns=[
            'Rolling Average', 'ARIMA', 'LinearRegression', 'RandomForest',
            'XGBoost'
        ])
    comparison = pd.DataFrame(
        index=['Smooth', 'Erratic', 'Intermittent', 'Lumpy'],
        columns=[
            'Rolling Average', 'ARIMA', 'LinearRegression', 'RandomForest',
            'XGBoost'
        ])

    for i in comparison.index:
      for x in comparison.columns:
        # Count number of times each model was the best to forecast an item
        if x in result[result[y] == i]['Best model'].value_counts():
          count_best.at[i, x] = result[result[y] ==
                                       i]['Best model'].value_counts()[x]
        else:
          count_best.at[i, x] = 0
        # Evaluate the mean errors for each forecasting model
        comparison.at[i, x] = result[result[y] == i][x].mean()

    results["{0}_comparison_{1}".format(y, el[:-4])] = comparison
    counts["{0}_count_best_{1}".format(y, el[:-4])] = count_best

  final_results = pd.concat(results.values(),
                            keys=['RMSE', 'NRMSE', 'MAE', 'R2', 'MAPE'])

  final_counts = pd.concat(counts.values(),
                           keys=['RMSE', 'NRMSE', 'MAE', 'R2', 'MAPE'])

  save_csv(final_results, '{0}_comparison.csv'.format(y))

  save_csv(final_counts, '{0}_count_best.csv'.format(y))