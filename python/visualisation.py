import pandas as pd
from pandas.core.frame import DataFrame
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import os
import pathlib
'''
This code is used to generate some useful visualisations of our results
'''


def save_plot(filename: str):
  filepath = os.path.join(os.getcwd(), '..', 'data', 'visualisation')
  pathlib.Path(filepath).mkdir(parents=True, exist_ok=True)
  plt.savefig(os.path.join(filepath, filename),
              transparent=False,
              facecolor='white')


def bar(df: pd.DataFrame, name: str):
  counts = pd.DataFrame(columns=[
      'Smooth', 'Intermittent', 'Erratic', 'Lumpy', 'Only ordered once',
      'Not ordered'
  ])

  counts = counts.append([
      df['2016'].value_counts(), df['2017'].value_counts(),
      df['2018'].value_counts(), df['2019'].value_counts(),
      df['2020'].value_counts(), df['2021'].value_counts(),
      df['Total'].value_counts()
  ])

  counts = counts.fillna(0)
  counts = counts.astype(int)

  category_names = counts.columns
  results = {
      '2016': counts.loc['2016'],
      '2017': counts.loc['2017'],
      '2018': counts.loc['2018'],
      '2019': counts.loc['2019'],
      '2020': counts.loc['2020'],
      '2021': counts.loc['2021'],
      'Total': counts.loc['Total']
  }

  labels = list(results.keys())
  data = np.array(list(results.values()))
  data_cum = data.cumsum(axis=1)
  category_colors = plt.get_cmap('cividis')(np.linspace(
      0.15, 0.85, data.shape[1]))

  fig, ax = plt.subplots(figsize=(9.2, 5))
  ax.invert_yaxis()
  ax.xaxis.set_visible(False)
  ax.set_xlim(0, np.sum(data, axis=1).max())
  fig.suptitle(name, fontsize=16)

  for i, (colname, color) in enumerate(zip(category_names, category_colors)):
    widths = data[:, i]
    starts = data_cum[:, i] - widths
    rects = ax.barh(labels,
                    widths,
                    left=starts,
                    height=0.5,
                    label=colname,
                    color=color)

    r, g, b, _ = color
    text_color = 'white' if r * g * b < 0.5 else 'darkgrey'
    ax.bar_label(rects, label_type='center', color=text_color)
    ax.legend(ncol=len(category_names),
              bbox_to_anchor=(0, 1),
              loc='lower left',
              fontsize='small')

  return fig, ax


def pie(df: pd.DataFrame, metric: str, title: str):
  # Make figure and axes
  fig, axs = plt.subplots(2, 2)
  labels = [
      'Rolling Average', 'ARIMA', 'LinearRegression', 'RandomForest', 'XGBoost'
  ]
  # A standard pie plot
  axs[0, 0].pie(df.loc[metric, 'Smooth'],
                autopct='%1.1f%%',
                shadow=True,
                explode=(0.1, 0, 0, 0, 0))
  axs[0, 0].set_title('Smooth')

  # Shift the second slice using explode
  axs[0, 1].pie(df.loc[metric, 'Erratic'],
                autopct='%1.1f%%',
                shadow=True,
                explode=(0.1, 0, 0, 0, 0))
  axs[0, 1].set_title('Erratic')

  # Adapt radius and text size for a smaller pie
  axs[1, 0].pie(df.loc[metric, 'Intermittent'],
                autopct='%1.1f%%',
                shadow=True,
                explode=(0.1, 0, 0, 0, 0))
  axs[1, 0].set_title('Intermittent')

  # Use a smaller explode and turn of the shadow for better visibility
  patches, texts, autotexts = axs[1, 1].pie(df.loc[metric, 'Lumpy'],
                                            autopct='%1.1f%%',
                                            shadow=True,
                                            explode=(0.1, 0, 0, 0, 0))
  axs[1, 1].set_title('Lumpy')

  fig.legend(patches,
             labels,
             title="Forecasting model",
             bbox_to_anchor=(1.15, 0.7))
  fig.suptitle(title, fontsize=16)

  return fig, axs


def scatter(x, y, title: str):
  fig, ax = plt.subplots()
  ax.scatter(x, y, s=1)
  plt.plot([0.49, 0.49], [1, 5], 'red')
  plt.plot([0, 5], [1.32, 1.32], 'red')
  plt.xlim([0, 5])
  plt.xlabel('CV2')
  plt.ylim([1, 5])
  plt.ylabel('ADI')
  fig.suptitle(title, fontsize=16)
  return fig, ax


data_path_monthly = os.path.join(os.getcwd(), '..', 'data', 'classification',
                                 'monthly_classification_overview.csv')
df_monthly = pd.read_csv(data_path_monthly, encoding='ISO-8859-1')

data_path_quarterly = os.path.join(os.getcwd(), '..', 'data', 'classification',
                                   'quarterly_classification_overview.csv')
df_quarterly = pd.read_csv(data_path_quarterly, encoding='ISO-8859-1')

data_path = os.path.join(os.getcwd(), '..', 'data', 'eval',
                         '2020_count_best.csv')
count_best_2020 = pd.read_csv(data_path, encoding='ISO-8859-1')
count_best_2020 = count_best_2020.set_index(['Unnamed: 0', 'Unnamed: 1'])

data_path = os.path.join(os.getcwd(), '..', 'data', 'eval',
                         '2020_comparison.csv')
comparison_2020 = pd.read_csv(data_path, encoding='ISO-8859-1')
comparison_2020 = comparison_2020.set_index(['Unnamed: 0', 'Unnamed: 1'])

data_path = os.path.join(os.getcwd(), '..', 'data', 'eval',
                         '2019_count_best.csv')
count_best_2019 = pd.read_csv(data_path, encoding='ISO-8859-1')
count_best_2019 = count_best_2019.set_index(['Unnamed: 0', 'Unnamed: 1'])

data_path = os.path.join(os.getcwd(), '..', 'data', 'eval',
                         '2019_comparison.csv')
comparison_2019 = pd.read_csv(data_path, encoding='ISO-8859-1')
comparison_2019 = comparison_2019.set_index(['Unnamed: 0', 'Unnamed: 1'])

data_path = os.path.join(os.getcwd(), '..', 'data', 'eval',
                         'Total_count_best.csv')
count_best_Total = pd.read_csv(data_path, encoding='ISO-8859-1')
count_best_Total = count_best_Total.set_index(['Unnamed: 0', 'Unnamed: 1'])

data_path = os.path.join(os.getcwd(), '..', 'data', 'eval',
                         'Total_comparison.csv')
comparison_Total = pd.read_csv(data_path, encoding='ISO-8859-1')
comparison_Total = comparison_Total.set_index(['Unnamed: 0', 'Unnamed: 1'])

data_path = os.path.join(os.getcwd(), '..', 'data', 'classification',
                         'classification_clean_Total.csv')
classification_Total = pd.read_csv(data_path, encoding='ISO-8859-1')

plt.show()

bar(df_quarterly, 'Demand Classification (quarterly)')
#save_plot('demand_classification_quarterly_bar.png')
bar(df_monthly, 'Demand Classification (monthly)')
#save_plot('demand_classification_monthly_bar.png')
pie(count_best_Total, 'NRMSE', 'NRMSE based on total classification')
pie(count_best_2020, 'NRMSE', 'NRMSE based on 2020 classification')
#pie(count_best_2019, 'NRMSE', 'NRMSE based on 2019 classification')
scatter(classification_Total['CV2 (quarterly)'],
        classification_Total['ADI (quarterly)'], 'Quarterly Classification')
scatter(classification_Total['CV2 (monthly)'],
        classification_Total['ADI (monthly)'], 'Monthly Classification')
plt.show()