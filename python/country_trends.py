import pandas as pd
import os
import pickle
import pathlib
import statsmodels.formula.api as smf
from statsmodels.iolib.summary2 import summary_col
import matplotlib.pyplot as plt

def save_csv(df: pd.DataFrame, filename: str):
  filepath = os.path.join(os.getcwd(), '..', 'data', 'csv')
  pathlib.Path(filepath).mkdir(parents=True, exist_ok=True)
  df.to_csv(os.path.join(filepath, filename))

# Load data
data_path = os.path.join(os.getcwd(), '..', 'data\csv', 'clean_without_drop.csv')
df = pd.read_csv(data_path, encoding='ISO-8859-1')

# Rename variables
df = df.rename(columns={
    'Ordered quantity': 'Quantity',
    'Name country delivered': 'Country'
})


#####################################################################################
# 1. Understand the pattern of quantity, price, mission  
#####################################################################################

# Aggregate data by country
aggregate_country_metrics = df.groupby([
    'Country',
    'Year',
    'Month'
]).agg(SumQty=('Quantity', 'sum'),
       SumPrice=('Orderlineprice', 'sum'),
       SumMission=('CodeClientDispatch', 'count'))
aggregate_country_metrics["Qtybinary"] = 0
aggregate_country_metrics["Qtybinary"][aggregate_country_metrics["SumQty"] >= 1] = 1
df_country = aggregate_country_metrics.reset_index()

# Create a OLS regression table for results
lm_Quantity = smf.ols(formula='SumQty ~  C(Month) + C(Year)', data=df_country).fit()
lm_Price = smf.ols(formula='SumPrice ~  C(Month) + C(Year)', data=df_country).fit()
lm_Mission = smf.ols(formula='SumMission ~  C(Month) + C(Year)', data=df_country).fit()
info_dict={'R-squared' : lambda x: f"{x.rsquared:.2f}",
           'No. observations' : lambda x: f"{int(x.nobs):d}"}
results_table = summary_col(results=[lm_Quantity, lm_Price, lm_Mission],
                            float_format='%0.2f',
                            stars = True,
                            model_names=['Quantity', "Price", "Mission"],
                            info_dict=info_dict)
results_table.add_title('OLS Regressions of quantity, price, and mission')
print(results_table)
# Quantity of supply slightly significantly lower in February, March, April, June, August, December than January as well as higher in 2017 relative to 2016.
# More money was spend in November relative to January - we do not see the December effect MSF assumes
# Less missions orderer in December compared to January, more in 2019 and 2021 compared to 2016.


#####################################################################################
# 2. Understand if certain countries have certain supply windows 
#####################################################################################

# Aggregate
df_country['MonthYear'] = df_country['Year'].astype(str) + "." + df_country['Month'].astype(str)
df_country_panel = df_country.pivot(index="Country", columns=['MonthYear'], values="Qtybinary")
df_country_panel = df_country_panel.fillna(0)
df_country_panel= df_country_panel.reset_index()

# Transfer to long format
df_country_long =pd.melt(df_country_panel, id_vars=['Country'],var_name='metrics', value_name='values')
df_country_long.head
df_country_long[['Year', 'Month']] = df_country_long['metrics'].str.split('.', 1, expand=True)

# Create OLS regression table
lm_Country = smf.ols(formula='values ~  C(Month)', data=df_country_long).fit()
info_dict={'R-squared' : lambda x: f"{x.rsquared:.2f}",
           'No. observations' : lambda x: f"{int(x.nobs):d}"}
results_table = summary_col(results=[lm_Country],
                            float_format='%0.2f',
                            stars = True,
                            model_names=['Country'],
                            info_dict=info_dict)
results_table.add_title('OLS Regressions of likelihood of at least one supply')
print(results_table)
# The likelihood of having at least one order seems not be significantly different among months

# Look at some specific countries
aggregate_country_metrics = df_country_long.groupby([
    'Country',
    'Month'
]).agg(SumOrder=('values', 'sum'))
aggregate_country_metrics = aggregate_country_metrics.reset_index()
aggregate_country_metrics = aggregate_country_metrics.pivot(index="Country", columns=['Month'], values="SumOrder")
aggregate_country_metrics = aggregate_country_metrics.reset_index()
aggregate_country_metrics["Total"] = aggregate_country_metrics["1"] + aggregate_country_metrics["2"] + aggregate_country_metrics["3"] + aggregate_country_metrics["4"] + aggregate_country_metrics["5"] + aggregate_country_metrics["6"] + aggregate_country_metrics["7"] + aggregate_country_metrics["8"] + aggregate_country_metrics["9"] + aggregate_country_metrics["10"] + aggregate_country_metrics["11"] + aggregate_country_metrics["12"] 
aggregate_country_metrics["Mean"] = (aggregate_country_metrics["1"] + aggregate_country_metrics["2"] + aggregate_country_metrics["3"] + aggregate_country_metrics["4"] + aggregate_country_metrics["5"] + aggregate_country_metrics["6"] + aggregate_country_metrics["7"] + aggregate_country_metrics["8"] + aggregate_country_metrics["9"] + aggregate_country_metrics["10"] + aggregate_country_metrics["11"] + aggregate_country_metrics["12"] ) / 12
save_csv(aggregate_country_metrics, 'aggregate_country_metrics.csv')

# Format Date 
df_country_long["Date"] = pd.to_datetime(df_country_long["metrics"]).dt.date

# Show one example of a country that seems to never order in December (but does not order since July 2020 anymore)
df_Malawi = df_country_long[df_country_long["Country"] == "Malawi"]
fig, axs = plt.subplots(figsize=(12, 4))
df_Malawi.groupby(df_Malawi["Date"])["values"].sum().plot(
    kind='bar', rot=0, ax=axs
)
plt.xticks(rotation=90, fontsize=5)
plt.xlabel("Date")
plt.ylabel("At least one order per Month, Malawi")
plt.show()

# Show one example of a country that seems to mostly order in May (but does not order since 2020 anymore)
df_Libya = df_country_long[df_country_long["Country"] == "State of Libya"]
fig, axs = plt.subplots(figsize=(12, 4))
df_Libya.groupby(df_Libya["Date"])["values"].sum().plot(
    kind='bar', rot=0, ax=axs
)
plt.xticks(rotation=90, fontsize=5)
plt.xlabel("Date")
plt.ylabel("At least one order per Month, Libya")
plt.show()



#####################################################################################
# 3. Re-run analysis with Dataset "clean", not including all data 
#####################################################################################
# Load data
data_path = os.path.join(os.getcwd(), '..', 'data\csv', 'clean.csv')
df = pd.read_csv(data_path, encoding='ISO-8859-1')

# Rename variables
df = df.rename(columns={
    'Ordered quantity': 'Quantity',
    'Name country delivered': 'Country'
})


# Aggregate data by country
aggregate_country_metrics = df.groupby([
    'Country',
    'Year',
    'Month'
]).agg(SumQty=('Quantity', 'sum'),
       SumPrice=('Orderlineprice', 'sum'),
       SumMission=('CodeClientDispatch', 'count'))
aggregate_country_metrics["Qtybinary"] = 0
aggregate_country_metrics["Qtybinary"][aggregate_country_metrics["SumQty"] >= 1] = 1
df_country = aggregate_country_metrics.reset_index()

# Create a OLS regression table for results
lm_Quantity = smf.ols(formula='SumQty ~  C(Month) + C(Year)', data=df_country).fit()
lm_Price = smf.ols(formula='SumPrice ~  C(Month) + C(Year)', data=df_country).fit()
lm_Mission = smf.ols(formula='SumMission ~  C(Month) + C(Year)', data=df_country).fit()
info_dict={'R-squared' : lambda x: f"{x.rsquared:.2f}",
           'No. observations' : lambda x: f"{int(x.nobs):d}"}
results_table = summary_col(results=[lm_Quantity, lm_Price, lm_Mission],
                            float_format='%0.2f',
                            stars = True,
                            model_names=['Quantity', "Price", "Mission"],
                            info_dict=info_dict)
results_table.add_title('OLS Regressions of quantity, price, and mission')
print(results_table)
# Effects stay similar than with the total data 



# Aggregate
df_country['MonthYear'] = df_country['Year'].astype(str) + "." + df_country['Month'].astype(str)
df_country_panel = df_country.pivot(index="Country", columns=['MonthYear'], values="Qtybinary")
df_country_panel = df_country_panel.fillna(0)
df_country_panel= df_country_panel.reset_index()

# Transfer to long format
df_country_long =pd.melt(df_country_panel, id_vars=['Country'],var_name='metrics', value_name='values')
df_country_long.head
df_country_long[['Year', 'Month']] = df_country_long['metrics'].str.split('.', 1, expand=True)

# Create OLS regression table
lm_Country = smf.ols(formula='values ~  C(Month)', data=df_country_long).fit()
info_dict={'R-squared' : lambda x: f"{x.rsquared:.2f}",
           'No. observations' : lambda x: f"{int(x.nobs):d}"}
results_table = summary_col(results=[lm_Country],
                            float_format='%0.2f',
                            stars = True,
                            model_names=['Country'],
                            info_dict=info_dict)
results_table.add_title('OLS Regressions of likelihood of at least one supply')
print(results_table)
# The likelihood of having at least one order seems not be significantly different among months

# Look at some specific countries
aggregate_country_metrics = df_country_long.groupby([
    'Country',
    'Month'
]).agg(SumOrder=('values', 'sum'))
aggregate_country_metrics = aggregate_country_metrics.reset_index()
aggregate_country_metrics = aggregate_country_metrics.pivot(index="Country", columns=['Month'], values="SumOrder")
aggregate_country_metrics = aggregate_country_metrics.reset_index()
aggregate_country_metrics["Total"] = aggregate_country_metrics["1"] + aggregate_country_metrics["2"] + aggregate_country_metrics["3"] + aggregate_country_metrics["4"] + aggregate_country_metrics["5"] + aggregate_country_metrics["6"] + aggregate_country_metrics["7"] + aggregate_country_metrics["8"] + aggregate_country_metrics["9"] + aggregate_country_metrics["10"] + aggregate_country_metrics["11"] + aggregate_country_metrics["12"] 
aggregate_country_metrics["Mean"] = (aggregate_country_metrics["1"] + aggregate_country_metrics["2"] + aggregate_country_metrics["3"] + aggregate_country_metrics["4"] + aggregate_country_metrics["5"] + aggregate_country_metrics["6"] + aggregate_country_metrics["7"] + aggregate_country_metrics["8"] + aggregate_country_metrics["9"] + aggregate_country_metrics["10"] + aggregate_country_metrics["11"] + aggregate_country_metrics["12"] ) / 12
save_csv(aggregate_country_metrics, 'aggregate_country_metrics.csv')

# Format Date 
df_country_long["Date"] = pd.to_datetime(df_country_long["metrics"]).dt.date

# Show one example of a country that seems to never order in December (but does not order since July 2020 anymore)
df_Malawi = df_country_long[df_country_long["Country"] == "Malawi"]
fig, axs = plt.subplots(figsize=(12, 4))
df_Malawi.groupby(df_Malawi["Date"])["values"].sum().plot(
    kind='bar', rot=0, ax=axs
)
plt.xticks(rotation=90, fontsize=5)
plt.xlabel("Date")
plt.ylabel("At least one order per Month, Malawi")
plt.show()