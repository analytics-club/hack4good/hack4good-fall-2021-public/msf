import pandas as pd
from pandas.core.frame import DataFrame
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib
import statsmodels.api as sm
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, mean_absolute_percentage_error
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from xgboost.sklearn import XGBRegressor
import warnings
'''
This code applies and compares several different forecasting models. The models used are:
- Rolling mean average of the last 6 months (which is what MSF is using at the moment)
- Linear regression
- Random Forest
- XGBoost
- ARIMA
We are also using several different metrics to compare the error, which are:
- Root mean squared error (RMSE)
- Normalised root mean squared error (NRMSE)
- Mean average error (MAE)
- R2-score
- Mean absolute percentage error (MAPE)
'''

warnings.filterwarnings('ignore')

TIME_WINDOW = 6  #The number of previous months that are used in the rolling average to forecast the next month
model_scores = {}


def save_csv(df: pd.DataFrame, filename: str):
  filepath = os.path.join(os.getcwd(), '..', 'data', 'forecasts')
  pathlib.Path(filepath).mkdir(parents=True, exist_ok=True)
  df.to_csv(os.path.join(filepath, filename))


# Calculate difference in sales month over month
def get_diff(data):
  data['sales_diff'] = data.sales.diff()
  data = data.dropna()
  return data


# Generating dataset used in the regressive models
def generate_supervised(data):
  supervised_df = data.copy()

  #create column for each lag
  for i in range(1, 13):
    col = 'lag_' + str(i)
    supervised_df[col] = supervised_df['sales_diff'].shift(i)

  #drop null values
  supervised_df = supervised_df.dropna().reset_index(drop=True)

  return supervised_df


def tts(data):
  """Splits the data into train and test. Test set consists of the last 12
    months of data.
    """
  data = data.drop(['sales'], axis=1)
  train, test = data[0:-12].values, data[-12:].values

  return train, test


def scale_data(train_set, test_set):
  """Scales data using MinMaxScaler and separates data into X_train, y_train,
    X_test, and y_test.
    Keyword Arguments:
    -- train_set: dataset used to train the model
    -- test_set: dataset used to test the model
    """

  #apply Min Max Scaler
  scaler = MinMaxScaler(feature_range=(-1, 1))
  scaler = scaler.fit(train_set)

  # reshape training set
  train_set = train_set.reshape(train_set.shape[0], train_set.shape[1])
  train_set_scaled = scaler.transform(train_set)

  # reshape test set
  test_set = test_set.reshape(test_set.shape[0], test_set.shape[1])
  test_set_scaled = scaler.transform(test_set)

  X_train, y_train = train_set_scaled[:, 1:], train_set_scaled[:, 0:1].ravel()
  X_test, y_test = test_set_scaled[:, 1:], test_set_scaled[:, 0:1].ravel()

  return X_train, y_train, X_test, y_test, scaler


def undo_scaling(y_pred, x_test, scaler_obj, lstm=False):
  """For visualizing and comparing results, undoes the scaling effect on
    predictions.
    Keyword arguments:
    -- y_pred: model predictions
    -- x_test: features from the test set used for predictions
    -- scaler_obj: the scaler objects used for min-max scaling
    -- lstm: indicate if the model run is the lstm. If True, additional
             transformation occurs
    """

  #reshape y_pred
  y_pred = y_pred.reshape(y_pred.shape[0], 1, 1)

  if not lstm:
    x_test = x_test.reshape(x_test.shape[0], 1, x_test.shape[1])

  #rebuild test set for inverse transform
  pred_test_set = []
  for index in range(0, len(y_pred)):
    pred_test_set.append(np.concatenate([y_pred[index], x_test[index]],
                                        axis=1))

  #reshape pred_test_set
  pred_test_set = np.array(pred_test_set)
  pred_test_set = pred_test_set.reshape(pred_test_set.shape[0],
                                        pred_test_set.shape[2])

  #inverse transform
  pred_test_set_inverted = scaler_obj.inverse_transform(pred_test_set)

  return pred_test_set_inverted


def predict_df(unscaled_predictions, original_df):
  """Generates a dataframe that shows the predicted sales for each month
    for plotting results.
    Keyword arguments:
    -- unscaled_predictions: the model predictions that do not have min-max or
                             other scaling applied
    -- original_df: the original monthly sales dataframe
    """
  #create dataframe that shows the predicted sales
  result_list = []
  sales_dates = list(original_df[-13:].index)
  act_sales = list(original_df[-13:].sales)
  unscaled_predictions = unscaled_predictions[-13:]

  for index in range(0, len(unscaled_predictions)):
    result_dict = {}
    result_dict['pred_value'] = int(
        pd.DataFrame(unscaled_predictions).iloc[index][0] + act_sales[index])
    if result_dict['pred_value'] < 0: result_dict['pred_value'] = 0
    result_dict['date'] = sales_dates[index]
    result_list.append(result_dict)

  df_result = pd.DataFrame(result_list)

  return df_result


def get_scores(unscaled_df, original_df, model_name):
  """Prints the root mean squared error, mean absolute error, and r2 scores
    for each model. Saves all results in a model_scores dictionary for
    comparison.
    Keyword arguments:
    -- unscaled_predictions: the model predictions that do not have min-max or
                             other scaling applied
    -- original_df: the original monthly sales dataframe
    -- model_name: the name that will be used to store model scores
    """
  rmse = np.sqrt(
      mean_squared_error(original_df.sales[-12:],
                         unscaled_df.pred_value[-12:]))
  mae = mean_absolute_error(original_df.sales[-12:],
                            unscaled_df.pred_value[-12:])
  r2 = r2_score(original_df.sales[-12:], unscaled_df.pred_value[-12:])
  mape = mean_absolute_percentage_error(original_df.sales[-12:],
                                        unscaled_df.pred_value[-12:])
  nrmse = rmse / original_df.sales[-12:].mean()
  model_scores[model_name] = [rmse, nrmse, mae, r2, mape]

  print(f"RMSE: {rmse}")
  print(f"NRMSE: {nrmse}")
  print(f"MAE: {mae}")
  print(f"R2 Score: {r2}")
  print(f"MAPE: {mape}")


def plot_results(results, original_df, model_name):
  """Plots predictions over original data to visualize results. Saves each
    plot as a png.
    Keyword arguments:
    -- results: a dataframe with unscaled predictions
    -- original_df: the original monthly sales dataframe
    -- model_name: the name that will be used in the plot title
    """
  fig, ax = plt.subplots(figsize=(15, 5))
  sns.lineplot(original_df.index,
               original_df.sales,
               data=original_df,
               ax=ax,
               label='Original',
               color='mediumblue')
  sns.lineplot(results.date,
               results.pred_value,
               data=results,
               ax=ax,
               label='Predicted',
               color='red')
  ax.set(xlabel="Date",
         ylabel="Sales",
         title=f"{model_name} Sales Forecasting Prediction")
  locs, labels = plt.xticks()
  plt.setp(labels, rotation=90)
  ax.legend()
  sns.despine()

  #plt.savefig(f'../model_output/{model_name}_forecast.png')


def rolling_average(data):
  idx = data[-13:].index
  result = pd.DataFrame(index=idx, columns=['pred_value', 'date'])
  for i in range(len(idx)):
    result.iloc[i]['pred_value'] = data.iloc[-13 + i - TIME_WINDOW:-13 +
                                             i]['sales'].mean()
  result.date = idx
  get_scores(result, data, 'Rolling Average')
  #plot_results(result, data, 'Rolling Average')


def sarimax_model(data, original_df):
  """Runs an arima model with 12 lags and yearly seasonal impact. Generates
    dynamic predictions for last 12 months. Prints and saves scores and plots
    results.
    """
  # Model
  sar = sm.tsa.statespace.SARIMAX(data.sales_diff,
                                  order=(12, 0, 0),
                                  seasonal_order=(0, 1, 0, 12),
                                  trend='c',
                                  enforce_stationarity=False).fit(disp=False)

  # Generate predictions
  start, end, dynamic = 40, 100, 7
  data['pred_value'] = sar.predict(start=start, end=end, dynamic=dynamic)

  # Generate predictions dataframe
  unscaled_df = predict_df(data, original_df)
  unscaled_df.pred_value *= 0.5

  # print scores and plot results
  get_scores(unscaled_df, original_df, 'ARIMA')
  #plot_results(unscaled_df, original_df, 'ARIMA')


def regressive_model(train_data, test_data, model, model_name, original_df):
  """Runs regressive models in SKlearn framework. First calls scale_data
    to split into X and y and scale the data. Then fits and predicts. Finally,
    predictions are unscaled, scores are printed, and results are plotted and
    saved.
    Keyword arguments:
    -- train_set: dataset used to train the model
    -- test_set: dataset used to test the model
    -- model: the sklearn model and model arguments in the form of
              model(kwarga)
    -- model_name: the name that will be used to store model scores and plotting
    """

  # Split into X & y and scale data
  X_train, y_train, X_test, y_test, scaler_object = scale_data(
      train_data, test_data)
  # Run sklearn models
  mod = model
  mod.fit(X_train, y_train)
  predictions = mod.predict(X_test)

  # Undo scaling to compare predictions against original data
  unscaled = undo_scaling(predictions, X_test, scaler_object)
  unscaled_df = predict_df(unscaled, original_df)

  # print scores and plot results
  get_scores(unscaled_df, original_df, model_name)
  #plot_results(unscaled_df, original_df, model_name)


YEARS = range(2016, 2022)
cnt = 0

data_path_products = os.path.join(os.getcwd(), '..', 'data', 'classification',
                                  'monthly_classification_overview.csv')
product_df = pd.read_csv(data_path_products, encoding='ISO-8859-1')

# Discard products that were not ordered at all in recent time from forecasting
product_df = product_df[product_df['2020'] != 'Not ordered']

ALL = len(product_df)
NUMBER_OF_ITEMS = ALL
products = list(product_df.iloc[:NUMBER_OF_ITEMS]['ArticleVersionLast'])

data_path_monthly = os.path.join(os.getcwd(), '..', 'data', 'csv', 'clean.csv')
df = pd.read_csv(data_path_monthly, encoding='ISO-8859-1')

df = df.set_index('ArticleVersionLast')

df = df[[
    'DescriptionArticleEn', 'Groupe name', 'Familly name', 'Date',
    'Date_quarter', 'Ordered quantity'
]]

months = []
for y in YEARS:
  for m in [
      '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'
  ]:
    months.append(str(y) + '-' + m)
months = months[:-2]

default = pd.DataFrame(index=months, columns=['sales'])
default['sales'] = 0

final_results = {}
for el in ['RMSE', 'NRMSE', 'MAE', 'R2', 'MAPE']:
  final_results[el] = pd.DataFrame(columns=[
      'Rolling Average', 'ARIMA', 'LinearRegression', 'RandomForest', 'XGBoost'
  ])

for art in products:
  cnt += 1
  print('-------- CURRENTLY WORKING ON ITEM ', art, cnt, ' OUT OF ',
        NUMBER_OF_ITEMS, ' --------')
  model_scores = {}
  item = default
  item['sales'] = df.loc[art].groupby('Date').sum()
  item = item.fillna(0)
  original = item.copy()

  stationary = get_diff(item)

  model_df = generate_supervised(stationary)
  train, test = tts(model_df)
  arima_data = stationary.drop('sales', axis=1)

  arima_data.index = pd.to_datetime(arima_data.index)

  rolling_average(item)

  sarimax_model(arima_data.iloc[:-13], original)

  regressive_model(train, test, LinearRegression(), 'LinearRegression',
                   original)
  regressive_model(train, test,
                   RandomForestRegressor(n_estimators=100, max_depth=20),
                   'RandomForest', original)
  regressive_model(
      train, test,
      XGBRegressor(n_estimators=100,
                   learning_rate=0.2,
                   objective='reg:squarederror'), 'XGBoost', original)
  temp = pd.DataFrame(model_scores,
                      index=['RMSE', 'NRMSE', 'MAE', 'R2', 'MAPE'])
  for el in temp.index:
    res = temp.loc[el]
    res = res.rename(art)
    final_results[el] = final_results[el].append(res)
for el in final_results:
  if el in ['RMSE', 'NRMSE', 'MAE', 'MAPE']:
    final_results[el]['Best model'] = final_results[el].idxmin(axis=1)
  else:
    final_results[el]['Best model'] = final_results[el].idxmax(axis=1)
  save_csv(final_results[el], '{}_scores.csv'.format(el))