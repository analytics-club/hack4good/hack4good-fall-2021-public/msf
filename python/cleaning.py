import pandas as pd
import os
import pickle
import pathlib
'''
This code serves to clean the original dataset. Items that should not be accounted for in out models are
discarded for various reasons:
- Too little data
- Not enough economic weight
- Urgent and therefore not part of the regular forecast
- Not ordered by MSF
'''

# The minimum amount of months for which an item needs to have some orders in order to be included in the dataset. Gets rid
# of items with too few orders
MIN_MONTHS_OF_DATA = 12

# The minumum total value of all orders of an item added up for it to be included in the dataset. Gets rid of items with
# little economic value
MIN_TOTAL_VALUE = 10000


# Save dataframe
def save_csv(df: pd.DataFrame, filename: str):
  filepath = os.path.join(os.getcwd(), '..', 'data', 'csv')
  pathlib.Path(filepath).mkdir(parents=True, exist_ok=True)
  df.to_csv(os.path.join(filepath, filename))


# Load data
data_path = os.path.join(os.getcwd(), '..', 'data', 'main.csv')
df = pd.read_csv(data_path, encoding='ISO-8859-1')

# Rename columns and replace values
df = df.replace({
    'Month': {
        'January': 1,
        'February': 2,
        'March': 3,
        'April': 4,
        'May': 5,
        'June': 6,
        'July': 7,
        'August': 8,
        'September': 9,
        'October': 10,
        'November': 11,
        'December': 12,
    }
})
df = df.rename(columns={
    'FamillyName': 'FamilyName',
    'Orderlineprice in ': 'Orderlineprice'
})

# Add monthly date column
df['Date'] = df.apply(lambda row: str(row['Year']) + '-' +
                      ('0' + str(row['Month'])
                       if len(str(row['Month'])) < 2 else str(row['Month'])),
                      axis=1)

# Add quarterly date column
df['Date_quarter'] = df.apply(
    lambda row: str(row['Year']) + '-' +
    ('0' + str(row['Quarter'])
     if len(str(row['Quarter'])) < 2 else str(row['Quarter'])),
    axis=1)

# Link articles to the most recent article version
version_map = df.groupby(['DescriptionArticleEn'
                          ]).apply(lambda x: x['ArticleVersion'].unique())
df['ArticleVersionLast'] = df.apply(
    lambda row: version_map[row['DescriptionArticleEn']][-1], axis=1)

# Save intermediate dataset
save_csv(df, 'clean_without_drop.csv')

# Find missions that are not from MSF but other NGO
df["MSF"] = 0
df.loc[df["CodeClientDipatch_Libellé"].str.contains(
    "MSF|Medecins sans|Medici|Doctors|Medicos s|bez hranic|decins sans|M.S.F|OCB",
    case=False), "MSF"] = 1

# Check prices and indicate potential outliers
df["PricePerItem"] = df["Orderlineprice"] / df["Ordered quantity"]
aggregate_price_metrics = df.groupby([
    'ArticleVersionLast',
    'DescriptionArticleEn',
]).agg(MinValue=('PricePerItem', 'min'),
       MeanValue=('PricePerItem', 'mean'),
       VarianceValue=('PricePerItem', 'var'))
aggregate_price_metrics["Potential_outlier"] = aggregate_price_metrics[
    "VarianceValue"] / aggregate_price_metrics["MeanValue"]
aggregate_price_metrics = aggregate_price_metrics.reset_index()
df = df.merge(aggregate_price_metrics,
              how="left",
              on=["ArticleVersionLast", "DescriptionArticleEn"])

# Not decided yet what to do with potential outliers

# Find articles that are not stocked by MSF
relevant_status = [
    'Stocked item', 'Stoped item', 'old stocked item to destock'
]

# Remove urgent orders
df = df[df['IsCommandeClientUrgente'] == 'No']

# Remove articles that are not stocked by MSF
df = df[df['Article status in EN'].isin(relevant_status)]

# Remove missions from other NGOs
df = df[df['MSF'] == 1]

# Group by month
by_month = df.groupby(['ArticleVersionLast', 'DescriptionArticleEn',
                       'Date']).sum()

# Compute aggregate metrics for quantity and price
aggregate_metrics = by_month.groupby([
    'ArticleVersionLast',
    'DescriptionArticleEn',
]).agg(MeanQty=('Ordered quantity', 'mean'),
       SumQty=('Ordered quantity', 'sum'),
       VarianceQty=('Ordered quantity', 'var'),
       MeanValue=('Orderlineprice', 'mean'),
       SumValue=('Orderlineprice', 'sum'),
       VarianceValue=('Orderlineprice', 'var'),
       Months=('Ordered quantity', 'count'))

# Find articles with sufficient data
sufficient_months = aggregate_metrics[
    aggregate_metrics['Months'] >= MIN_MONTHS_OF_DATA].reset_index(
    )['ArticleVersionLast'].unique()

# Find articles with sufficient total order value
sufficient_value = aggregate_metrics[
    aggregate_metrics['SumValue'] > MIN_TOTAL_VALUE].reset_index(
    )['ArticleVersionLast'].unique()

# Remove articles with too low value or too little data
df = df[df['ArticleVersionLast'].isin(sufficient_months)
        & df['ArticleVersionLast'].isin(sufficient_value)]

save_csv(df, 'clean.csv')