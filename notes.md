# MSF Demand Forecasting
## Notes About the Code
The code is split into several parts to make it more understandable. Different code files create different outputs that are used in other code files. It thus is important to execute the code in the right order, as shown in the flow chart. The generated csv files and png images will be automatically stored in the correct folders and can be accessed for more detailed analysis.

![Code Flowchart](/pics/MSF_flowchart.png)
## Demand Categorisation
- MSF is not the first organisation facing this problem and we are not the first ones trying to solve it so there is already quite some information available.
- With demand forecasting it is common practice to divide items into different categories based on their variability in demand quantity and average demand interval.
- You thus end up with 4 categories: Smooth, Intermittent, Erratic, and Lumpy
- You would expect smooth products to be more easily forecastable than the other ones
- Why is that good to know?
  - If you know that a certain proportion of your items belongs to either of these categories, it makes it easier to judge what kind of results you can expect. You would want as many items as possible to be smooth in order to be able to forecast them. Items from the other categories may be much harder to predict
- We followed the approach commonly accepted in the literature to classify our items.
- Obviously, the longer the timebucket you are working, the smoother your demand gets.
- Bucketing the items by month, you get way fewer smooth items than when you bucket by quarter for example.
- In order to get reasonable results we cleaned up the dataset and discarded items that had too little demand
- Here is the classifications we were able to extract:

| ![Monthly classification](/pics/demand_classification_monthly_bar.png) |
| :--------------------------------------------------------------------: |
|                          *On a monthly basis*                          |

| ![Quarterly classification](/pics/demand_classification_quarterly_bar.png) |
| :------------------------------------------------------------------------: |
|                           *On a quarterly basis*                           |

- As already mentioned and confirmed here, there are many more smooth items in the quarterly classification.
- What is also evident is that with a longer timeframe, items are more likely to appear smooth, as seen in the last bar.
---
## Difference between the Classes
- How are these classes actually different now? This becomes clear when looking at how the current model used by MSF perfoms on them. We are using the normalised root mean square error as a metric here to account for large differences in the total ordered quantity for different items:

| ![Quarterly classification](/pics/Comparison_of_RollingMean_2020.png) |
| :-------------------------------------------------------------------: |
|            *Performance of model on different categories*             |

- Same goes for other models that are applied to the items. Our classification into these different categories thus helps MSF figure out which items are more easily forecastable and also on which timescale this makes sense. Much fewer items can be forecasted easily on a monthly basis than on a quarterly basis.
